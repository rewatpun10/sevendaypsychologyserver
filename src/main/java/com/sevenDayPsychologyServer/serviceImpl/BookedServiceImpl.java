package com.sevenDayPsychologyServer.serviceImpl;

import com.sevenDayPsychologyServer.dto.ClientBookingDTO;
import com.sevenDayPsychologyServer.entities.BookedAppointment;
import com.sevenDayPsychologyServer.entities.BookingAvailability;
import com.sevenDayPsychologyServer.entities.User;
import com.sevenDayPsychologyServer.repository.BookedRepository;
import com.sevenDayPsychologyServer.service.BookedService;
import com.sevenDayPsychologyServer.service.BookingService;
import com.sevenDayPsychologyServer.service.UserService;
import com.sevenDayPsychologyServer.utility.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class BookedServiceImpl implements BookedService {

    @Autowired
    BookedRepository bookedRepository;
    @Autowired
    BookingService bookingService;
    @Autowired
    EmailService emailService;
    @Autowired
    UserService userService;
    @Override
    public BookedAppointment saveClientBooking(ClientBookingDTO clientBookingDTO) {
        BookedAppointment bookedAppointment = new BookedAppointment();
        bookedAppointment.setClientId(clientBookingDTO.getClientId());
        bookedAppointment.setBookingAvailabilityId(clientBookingDTO.getId());
        BookedAppointment bookedAppointment1 = bookedRepository.save(bookedAppointment);
        if(bookedAppointment1 != null) {
            bookingService.updateBookingAvailabilityStatus(clientBookingDTO.getId());
            User counsellorUser = userService.getUser(clientBookingDTO.getCounsellorId());
            User clientUser = userService.getUser(clientBookingDTO.getClientId());
            emailService.sendEmail(counsellorUser.getEmail(), clientUser.getEmail(), clientBookingDTO.getAvailabilityTime());
        }
        return bookedAppointment;
    }

    @Override
    public List<BookingAvailability> getBookedDetails(Long clientId) {
      return bookingService.getAllBookedAvailableAppointments(clientId);

    }
}
