package com.sevenDayPsychologyServer.controller;

import com.sevenDayPsychologyServer.dto.BookedUserDTO;
import com.sevenDayPsychologyServer.dto.UserDTO;
import com.sevenDayPsychologyServer.entities.User;
import com.sevenDayPsychologyServer.service.UserService;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.io.IOException;
import java.util.List;

@RestController
@CrossOrigin
public class UserController {

    @Autowired
    UserService userService;

    @PostMapping("/createUser")
    public String createUser(@RequestBody User user) {
        return userService.createUser(user);

    }

    @PutMapping("/updateUser")
    public boolean updateUser(@RequestBody UserDTO userDTO) {
        return userService.updateUser(userDTO);
    }

    @GetMapping("/getUserById/{id}")
    public UserDTO getUserByUserId(@PathVariable(value = "id") Long id) {
       return userService.getUserById(id);
    }

    @GetMapping("/getAllCounsellors")
    public List<UserDTO> getAllCounsellors() {
        return userService.getAllCounsellors();
    }

    @GetMapping("/getAllClients")
    public List<UserDTO> getAllClients() {
        return userService.getAllClients();
    }

    @DeleteMapping("/deleteUser/{id}")
    public HttpStatus deleteUser(@PathVariable(value = "id") Long id) {
        return userService.deleteUser(id);
    }

    @GetMapping("/getBookedAppointmentUserDetails/{id}")
    public BookedUserDTO getBookedAppointmentUserDetails(@PathVariable(value = "id") Long id) {
        return userService.getBookedUserDetails(id);
    }

    @GetMapping("/downloadExcelFileOfCounsellor")
    public ResponseEntity<InputStreamResource> downloadExcelFileOfCounsellors() throws IOException {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "attachment; filename=customers.xlsx");

        return ResponseEntity
                .ok()
                .headers(headers)
                .body(new InputStreamResource(userService.downloadExcelFileOfCounsellors()));
    }

    @GetMapping("/downloadExcelFileOfClient")
    public ResponseEntity<InputStreamResource> downloadExcelFileOfClient() throws IOException {
       HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "attachment; filename=customers.xlsx");

        return ResponseEntity
                .ok()
                .headers(headers)
                .body(new InputStreamResource(userService.downloadExcelFileOfClient()));
    }


}
