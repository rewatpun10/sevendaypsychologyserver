package com.sevenDayPsychologyServer.serviceImpl;

import com.sevenDayPsychologyServer.dto.BookingAvailableDTO;
import com.sevenDayPsychologyServer.dto.CounsellorBookingAvailableDTO;
import com.sevenDayPsychologyServer.entities.BookedAppointment;
import com.sevenDayPsychologyServer.entities.BookingAvailability;
import com.sevenDayPsychologyServer.entities.User;
import com.sevenDayPsychologyServer.enumeration.Roles;
import com.sevenDayPsychologyServer.repository.BookedRepository;
import com.sevenDayPsychologyServer.repository.BookingRepository;
import com.sevenDayPsychologyServer.repository.UserRepository;
import com.sevenDayPsychologyServer.service.BookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;


@Service
@Transactional
public class BookingServiceImpl implements BookingService {

    @Autowired
    BookingRepository bookingRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    BookedRepository bookedRepository;

    BookingAvailableDTO savedBookingAvailableDto = new BookingAvailableDTO();
    List<String> bookedSavedDate = new ArrayList<>();
    @Override
    public BookingAvailableDTO createAvailability(BookingAvailableDTO bookingDetails) {
        for(String date: bookingDetails.getAvailabilityDate()) {
            BookingAvailability bookingAvailability = new BookingAvailability();
            bookingAvailability.setCounsellorId(bookingDetails.getCounsellorId());
            bookingAvailability.setAvailabilityTime(date);
            bookedSavedDate.add(date);
            savedBookingAvailableDto.setCounsellorId(bookingDetails.getCounsellorId());
            bookingRepository.save(bookingAvailability);
        }
            savedBookingAvailableDto.setAvailabilityDate(bookedSavedDate);
            return bookingDetails;
    }

    @Override
    public List<BookingAvailability> getBookedAppointmentByCounsellorId(Long id) {
        List<BookingAvailability> bookingAvailabilities = bookingRepository.findAllByCounsellorId(id);
        return bookingAvailabilities;
    }

    @Override
    public void deleteAvailabilityById(Long id) {
        bookingRepository.deleteById(id);
    }

    @Override
    public List<BookingAvailability> getAllAvailableBookingAppointment() {
        return bookingRepository.findBookingAvailabilityByStatusIsFalse();
    }

    @Override
    public void updateBookingAvailabilityStatus(Long id) {
        BookingAvailability bookingAvailability = bookingRepository.getOne(id);
        bookingAvailability.setStatus(true);
        bookingRepository.save(bookingAvailability);
    }

    @Override
    public List<BookingAvailability> getAllBookedAvailableAppointments(Long clientId) {
        User user = userRepository.findDistinctById(clientId);
        List<BookedAppointment> bookedRepositoryList = new ArrayList<>();
        if (user.getRole().equalsIgnoreCase(String.valueOf(Roles.ADMIN))) {
            bookedRepositoryList = bookedRepository.findAll();
        }else if(user.getRole().equalsIgnoreCase(String.valueOf(Roles.CLIENT))) {
            bookedRepositoryList = bookedRepository.getAllByClientId(clientId);
        }else if(user.getRole().equalsIgnoreCase(String.valueOf(Roles.COUNSELLOR))){
            List<BookingAvailability> bookingAvailabilityList = bookingRepository.findAllByCounsellorId(clientId);
            for (BookingAvailability bookingAvailability: bookingAvailabilityList) {
                BookedAppointment bookedAppointment = new BookedAppointment();
                 bookedAppointment = bookedRepository.getBookedAppointmentByBookingAvailabilityId(bookingAvailability.getId());
                if(bookedAppointment!= null) {
                    bookedRepositoryList.add(bookedAppointment);
                }

            }
        }
        List<BookingAvailability> bookingAvailabilitiesList = new ArrayList<>();
        for (BookedAppointment bookedAppointment: bookedRepositoryList) {
            BookingAvailability bookingAvailability =  bookingRepository.findBookingAvailabilityById(bookedAppointment.getBookingAvailabilityId());
           if(bookingAvailability != null){
               bookingAvailabilitiesList.add(bookingAvailability);
           }
        }
        return bookingAvailabilitiesList;
    }
}
