package com.sevenDayPsychologyServer.service;

import com.sevenDayPsychologyServer.entities.Client;

public interface ClientService {
    Client createClient(Client Client);
}
