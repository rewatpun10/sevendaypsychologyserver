package com.sevenDayPsychologyServer.controller;

import com.sevenDayPsychologyServer.dto.BookingAvailableDTO;
import com.sevenDayPsychologyServer.dto.ClientBookingDTO;
import com.sevenDayPsychologyServer.dto.CounsellorBookingAvailableDTO;
import com.sevenDayPsychologyServer.dto.UserDTO;
import com.sevenDayPsychologyServer.entities.BookedAppointment;
import com.sevenDayPsychologyServer.entities.BookingAvailability;
import com.sevenDayPsychologyServer.entities.Client;
import com.sevenDayPsychologyServer.service.BookedService;
import com.sevenDayPsychologyServer.service.BookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin
public class BookingController {
    @Autowired
    private BookingService bookingService;
    @Autowired
    private BookedService bookedService;

    @PostMapping("/createBookingAvailability")
    public BookingAvailableDTO createUser(@RequestBody BookingAvailableDTO bookingAvailability) {
        return bookingService.createAvailability(bookingAvailability);
    }

    @GetMapping("/getBookedDateByCounsellorId/{id}")
    public List<CounsellorBookingAvailableDTO> getBookedAppointmentByCounsellorId(@PathVariable(value = "id") Long id) {
        List<CounsellorBookingAvailableDTO> counsellorBookingAvailableDTOS = new ArrayList<>();
        List<BookingAvailability> bookingAvailabilities = bookingService.getBookedAppointmentByCounsellorId(id);
        for (BookingAvailability bookingAvailability : bookingAvailabilities) {
            CounsellorBookingAvailableDTO counsellorBookingAvailableDTO = new CounsellorBookingAvailableDTO();
            counsellorBookingAvailableDTO.setCounsellorId(bookingAvailability.getCounsellorId());
            counsellorBookingAvailableDTO.setId(bookingAvailability.getId());
            counsellorBookingAvailableDTO.setAvailabilityDate(bookingAvailability.getAvailabilityTime());
            counsellorBookingAvailableDTOS.add(counsellorBookingAvailableDTO);
        }
    return counsellorBookingAvailableDTOS;
    }

    @DeleteMapping("/deleteBookedDateById/{id}")
    public HttpStatus deleteBookingAvailabilityByCounsellorId(@PathVariable(value = "id") Long id) {
        bookingService.deleteAvailabilityById(id);
        return HttpStatus.OK;
    }

    @GetMapping("/availableBookedDates")
    public List<BookingAvailability> getAllBookingAvailable(){
        List<BookingAvailability> bookingAvailabilities = bookingService.getAllAvailableBookingAppointment();
        return bookingAvailabilities;
    }

    @PostMapping("/bookAppointmentForClient")
    public BookedAppointment saveClientBooking(@RequestBody ClientBookingDTO clientBookingDTO){
       return bookedService.saveClientBooking(clientBookingDTO);
    }

    @GetMapping("/clientBookedDatesById/{id}")
    public List<BookingAvailability> getClientBookedDates(@PathVariable(value = "id") Long id){
        return bookedService.getBookedDetails(id);
    }
}
