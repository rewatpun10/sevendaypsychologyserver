package com.sevenDayPsychologyServer.utility;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class EmailService {

    @Autowired
    private JavaMailSender javaMailSender;
    //message confirmation to client and counsellor
    public void sendEmail(String counsellorEmail, String clientEmail, String appointmentDate) {
        StringBuilder sb = new StringBuilder();
        String[] strings =  {counsellorEmail , clientEmail};
        SimpleMailMessage msg = new SimpleMailMessage();
        msg.setTo(strings);
        msg.setSubject("Booking Appointment confirmation");
        msg.setText("Booking Date Appointment of " + appointmentDate + " has been scheduled.");
        javaMailSender.send(msg);

    }


}
