package com.sevenDayPsychologyServer.controller;

import com.sevenDayPsychologyServer.dto.UserDTO;
import com.sevenDayPsychologyServer.entities.User;
import com.sevenDayPsychologyServer.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class LoginController {

    @Autowired
    private UserService userService;
    @RequestMapping("/login")
    public UserDTO login(@RequestBody User user) {
        return userService.checkUserExists(user);
    }
}
