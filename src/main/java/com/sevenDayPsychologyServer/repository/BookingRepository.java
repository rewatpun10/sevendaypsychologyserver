package com.sevenDayPsychologyServer.repository;

import com.sevenDayPsychologyServer.entities.BookingAvailability;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BookingRepository extends JpaRepository<BookingAvailability, Long> {
    List<BookingAvailability> findAllByCounsellorId(Long id);

    void deleteById(Long id);

    List<BookingAvailability> findBookingAvailabilityByStatusIsFalse();

    BookingAvailability findBookingAvailabilityById(Long id);
}
