package com.sevenDayPsychologyServer.repository;

import com.sevenDayPsychologyServer.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsernameAndPassword(String username, String password);
    User findByUsername(String username);
    User findDistinctById(Long id);
    @Query("SELECT u FROM User u WHERE u.role = ?1 and u.status = ?2")
    List<User> findAllByRoleAndStatus(String role, boolean status);
}
