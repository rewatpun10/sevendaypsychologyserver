package com.sevenDayPsychologyServer.repository;

import com.sevenDayPsychologyServer.entities.Counsellor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CounsellorRepository extends JpaRepository<Counsellor, Long> {
    Counsellor findCounsellorByUserId(Long id);
}
