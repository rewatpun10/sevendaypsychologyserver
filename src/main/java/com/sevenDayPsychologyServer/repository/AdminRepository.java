package com.sevenDayPsychologyServer.repository;

import com.sevenDayPsychologyServer.entities.Admin;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AdminRepository extends JpaRepository<Admin, Long> {
    Admin findAdminByUserId(Long id);
}
