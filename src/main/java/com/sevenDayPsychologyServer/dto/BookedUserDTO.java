package com.sevenDayPsychologyServer.dto;

public class BookedUserDTO {
    private String counellorName;
    private String counsellorEmail;
    private String counsellorPhoneNumber;
    private String clientName;
    private String clientEmail;
    private String clientPhoneNumber;
    private String availableTime;

    public String getCounellorName() {
        return counellorName;
    }

    public void setCounellorName(String counellorName) {
        this.counellorName = counellorName;
    }

    public String getCounsellorEmail() {
        return counsellorEmail;
    }

    public void setCounsellorEmail(String counsellorEmail) {
        this.counsellorEmail = counsellorEmail;
    }

    public String getCounsellorPhoneNumber() {
        return counsellorPhoneNumber;
    }

    public void setCounsellorPhoneNumber(String counsellorPhoneNumber) {
        this.counsellorPhoneNumber = counsellorPhoneNumber;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getClientEmail() {
        return clientEmail;
    }

    public void setClientEmail(String clientEmail) {
        this.clientEmail = clientEmail;
    }

    public String getClientPhoneNumber() {
        return clientPhoneNumber;
    }

    public String getAvailableTime() {
        return availableTime;
    }

    public void setAvailableTime(String availableTime) {
        this.availableTime = availableTime;
    }

    public void setClientPhoneNumber(String clientPhoneNumber) {
        this.clientPhoneNumber = clientPhoneNumber;


    }
}
