package com.sevenDayPsychologyServer.service;

import com.sevenDayPsychologyServer.dto.BookedUserDTO;
import com.sevenDayPsychologyServer.dto.UserDTO;
import com.sevenDayPsychologyServer.entities.User;
import org.springframework.http.HttpStatus;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

public interface UserService {

String createUser(User user);

UserDTO checkUserExists(User user);

UserDTO getUserById(Long Id);

User getUser(Long id);

List<UserDTO> getAllCounsellors();

boolean updateUser(UserDTO userDTO);

List<UserDTO> getAllClients();

HttpStatus deleteUser(Long id);

BookedUserDTO getBookedUserDetails(Long id);

ByteArrayInputStream downloadExcelFileOfCounsellors() throws IOException;

ByteArrayInputStream downloadExcelFileOfClient() throws IOException;

}
