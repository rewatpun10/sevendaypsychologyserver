package com.sevenDayPsychologyServer.repository;

import com.sevenDayPsychologyServer.dto.BookedUserDTO;
import com.sevenDayPsychologyServer.entities.BookedAppointment;
import org.hibernate.sql.Select;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import javax.persistence.criteria.From;
import java.util.List;

public interface BookedRepository extends JpaRepository<BookedAppointment, Long> {

    List<BookedAppointment> getAllByClientId(Long clientId);

//    @Query(value = "select ba from BookedAppointment ba where ba.booking_availability_id = ?1")
    BookedAppointment getBookedAppointmentByBookingAvailabilityId(Long id);



}
