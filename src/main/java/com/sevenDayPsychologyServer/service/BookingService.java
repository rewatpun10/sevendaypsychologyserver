package com.sevenDayPsychologyServer.service;

import com.sevenDayPsychologyServer.dto.BookingAvailableDTO;
import com.sevenDayPsychologyServer.dto.ClientBookingDTO;
import com.sevenDayPsychologyServer.dto.CounsellorBookingAvailableDTO;
import com.sevenDayPsychologyServer.entities.BookingAvailability;

import java.util.List;

public interface BookingService {
    BookingAvailableDTO createAvailability(BookingAvailableDTO bookingDetails);

    List<BookingAvailability> getBookedAppointmentByCounsellorId(Long id);

    void deleteAvailabilityById(Long id);

    List<BookingAvailability> getAllAvailableBookingAppointment();

    void updateBookingAvailabilityStatus(Long id);

    List<BookingAvailability> getAllBookedAvailableAppointments(Long id);
}
