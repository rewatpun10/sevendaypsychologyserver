package com.sevenDayPsychologyServer.dto;

import java.util.Date;
import java.util.List;

public class BookingAvailableDTO {
    private List<String> availabilityDate;
    private Long counsellorId;

    public List<String> getAvailabilityDate() {
        return availabilityDate;
    }

    public void setAvailabilityDate(List<String> availabilityDate) {
        this.availabilityDate = availabilityDate;
    }

    public Long getCounsellorId() {
        return counsellorId;
    }

    public void setCounsellorId(Long counsellorId) {
        this.counsellorId = counsellorId;
    }


}
