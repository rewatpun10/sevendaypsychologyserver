package com.sevenDayPsychologyServer.entities;

import javax.persistence.*;

@Entity
@Table(name = "booked_appointment")
public class BookedAppointment {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "booked_id")
    private Long book_id;

    private Long clientId;

    private Long bookingAvailabilityId;

    public Long getBook_id() {
        return book_id;
    }

    public void setBook_id(Long book_id) {
        this.book_id = book_id;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public Long getBookingAvailabilityId() {
        return bookingAvailabilityId;
    }

    public void setBookingAvailabilityId(Long bookingAvailabilityId) {
        this.bookingAvailabilityId = bookingAvailabilityId;
    }
}
