package com.sevenDayPsychologyServer.serviceImpl;

import com.sevenDayPsychologyServer.dto.BookedUserDTO;
import com.sevenDayPsychologyServer.dto.UserDTO;
import com.sevenDayPsychologyServer.entities.*;
import com.sevenDayPsychologyServer.enumeration.Roles;
import com.sevenDayPsychologyServer.repository.*;
import com.sevenDayPsychologyServer.service.UserService;
import com.sevenDayPsychologyServer.utility.ExcelFileClient;
import com.sevenDayPsychologyServer.utility.ExcelFileCounsellor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    //Autowired injection
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ClientRepository clientRepository;
    @Autowired
    private CounsellorRepository counsellorRepository;
    @Autowired
    private AdminRepository adminRepository;
    @Autowired
    private BookedRepository bookedRepository;
    @Autowired
    private BookingRepository bookingRepository;
    @Autowired
    private ExcelFileCounsellor excelFileCounsellor;
    @Autowired
    private ExcelFileClient excelFileClient;

    //this method helps to register new user in the system based on their roles
    @Override
    public String createUser(User user) {
        String createUser = new String();
        User userExists = userRepository.findByUsername(user.getUsername());
        if(userExists!=null){
            createUser = "duplicate";
        }
        if(userExists == null && user.getRole().equalsIgnoreCase(String.valueOf(Roles.CLIENT))) {
            Client client = new Client();
            client.setUser(user);
            clientRepository.save(client);
            userRepository.save(user);
            createUser = "user created successfully";
        } else if( userExists == null && user.getRole().equalsIgnoreCase(String.valueOf(Roles.ADMIN))){
            Counsellor counsellor = new Counsellor();
            counsellor.setUser(user);
            counsellorRepository.save(counsellor);
            userRepository.save(user);
            createUser = "user created successfully";
        }
        return createUser;
    }

    //this method checks the user credentials and if matches send the user details
    @Override
    public UserDTO checkUserExists(User userDetails) {
        UserDTO userDTO = new UserDTO();
        User user = userRepository.findByUsernameAndPassword(userDetails.getUsername(), userDetails.getPassword());
        if (getUserDTO(userDTO, user)) return null;
        return userDTO;
    }

    private boolean getUserDTO(UserDTO userDTO, User user) {
        Client client;
        Counsellor counsellor;
        Admin admin;
        if(user == null) {
            return true;
        }else {
            userDTO.setUsername(user.getUsername());
            userDTO.setEmail(user.getEmail());
            userDTO.setRole(user.getRole());
            userDTO.setLoggedIn(true);
            userDTO.setId(user.getId());
            if(user.getRole().equalsIgnoreCase(String.valueOf(Roles.CLIENT))) {
                client = clientRepository.findClientByUserId(user.getId());
                userDTO.setFirstname(client.getFirstName());
                userDTO.setMiddlename(client.getMiddleName());
                userDTO.setLastname(client.getLastName());
                userDTO.setMobilenumber(client.getMobileNumber());
            }else if (user.getRole().equalsIgnoreCase(String.valueOf(Roles.COUNSELLOR))) {
               counsellor = counsellorRepository.findCounsellorByUserId(user.getId());
                userDTO.setFirstname(counsellor.getFirstName());
                userDTO.setMiddlename(counsellor.getMiddleName());
                userDTO.setLastname(counsellor.getLastName());
                userDTO.setMobilenumber(counsellor.getMobileNumber());
                userDTO.setDescription(counsellor.getDescription());
            } else if(user.getRole().equalsIgnoreCase(String.valueOf(Roles.ADMIN))) {
                admin = adminRepository.findAdminByUserId(user.getId());
                if(admin!=null) {
                    userDTO.setFirstname(admin.getFirstName());
                    userDTO.setMiddlename(admin.getMiddleName());
                    userDTO.setLastname(admin.getLastName());
                    userDTO.setMobilenumber(admin.getMobileNumber());
                }
            }
        }
        return false;
    }

    @Override
    public UserDTO getUserById(Long id) {
        UserDTO userDTO = new UserDTO();
        User user = userRepository.findDistinctById(id);
            if (getUserDTO(userDTO, user)) return null;
            return userDTO;
    }

    @Override
    public User getUser(Long id) {
        return userRepository.getOne(id);
    }

    @Override
    public List<UserDTO> getAllCounsellors() {
        boolean value = false;
        List<User> userList = userRepository.findAllByRoleAndStatus("counsellor", value);
        List<UserDTO> userDTOList = new ArrayList<>();
        for (User user: userList) {
            UserDTO userDTO = new UserDTO();
            getUserDTO(userDTO, user);
            userDTOList.add(userDTO);
        }
        return userDTOList;
    }

    @Override
    public boolean updateUser(UserDTO userDTO) {
        boolean result = false;
        Client client;
        Counsellor counsellor;
        Admin admin;
        User user = userRepository.findDistinctById(userDTO.getId());
        user.setEmail(userDTO.getEmail());
        user.setUsername(userDTO.getUsername());
        if(userDTO.getPassword() != null && !userDTO.getPassword().equals(user.getPassword())){
            user.setPassword(userDTO.getPassword());
        }
        userRepository.save(user);
        if(user.getRole().equalsIgnoreCase(String.valueOf(Roles.CLIENT))) {
            client = clientRepository.findClientByUserId(user.getId());
            client.setUser(user);
            client.setFirstName(userDTO.getFirstname());
            client.setMiddleName(userDTO.getMiddlename());
            client.setLastName(userDTO.getLastname());
            client.setMobileNumber(userDTO.getMobilenumber());
            clientRepository.save(client);
            result = true;
        }else if (user.getRole().equalsIgnoreCase(String.valueOf(Roles.COUNSELLOR))) {
            counsellor = counsellorRepository.findCounsellorByUserId(user.getId());
            counsellor.setFirstName(userDTO.getFirstname());
            counsellor.setMiddleName(userDTO.getMiddlename());
            counsellor.setLastName(userDTO.getLastname());
            counsellor.setMobileNumber(userDTO.getMobilenumber());
            counsellor.setDescription(userDTO.getDescription());
            counsellorRepository.save(counsellor);
            result = true;
        }
        else if( user.getRole().equalsIgnoreCase(String.valueOf(Roles.ADMIN))){
            admin = adminRepository.findAdminByUserId(user.getId());
            if(admin == null) {
                Admin admin1 = new Admin();
                admin1.setUser(user);
                admin1.setFirstName(userDTO.getFirstname());
                admin1.setMiddleName(userDTO.getMiddlename());
                admin1.setLastName(userDTO.getLastname());
                admin1.setMobileNumber(userDTO.getMobilenumber());
                adminRepository.save(admin1);
                result = true;
            } else {
                admin.setFirstName(userDTO.getFirstname());
                admin.setMiddleName(userDTO.getMiddlename());
                admin.setLastName(userDTO.getLastname());
                admin.setMobileNumber(userDTO.getMobilenumber());
                adminRepository.save(admin);
                result = true;
            }

        }
    return result;
    }

    @Override
    public List<UserDTO> getAllClients() {
        boolean value = false;
        List<User> userList = userRepository.findAllByRoleAndStatus("client", value);
        List<UserDTO> userDTOList = new ArrayList<>();
        for (User user: userList) {
            UserDTO userDTO = new UserDTO();
            getUserDTO(userDTO, user);
            userDTOList.add(userDTO);
        }
        return userDTOList;
    }

    @Override
    public HttpStatus deleteUser(Long id) {
        User user = userRepository.findDistinctById(id);
        user.setStatus(true);
        userRepository.save(user);
        return HttpStatus.OK;
    }

    @Override
    public BookedUserDTO getBookedUserDetails(Long id) {
        BookedUserDTO bookedUserDTO = new BookedUserDTO();
        BookedAppointment bookedAppointment = bookedRepository.getBookedAppointmentByBookingAvailabilityId(id);
        Client client = clientRepository.findClientByUserId(bookedAppointment.getClientId());
        BookingAvailability bookingAvailability = bookingRepository.findBookingAvailabilityById(id);
        Counsellor counsellor = counsellorRepository.findCounsellorByUserId(bookingAvailability.getCounsellorId());
        bookedUserDTO.setCounellorName(counsellor.getFirstName()+" " + counsellor.getMiddleName()+" " + counsellor.getLastName());
        bookedUserDTO.setCounsellorEmail(counsellor.getUser().getEmail());
        bookedUserDTO.setCounsellorPhoneNumber(counsellor.getMobileNumber());
        bookedUserDTO.setClientName(client.getFirstName()+" " + client.getMiddleName()+ " " + client.getLastName());
        bookedUserDTO.setClientEmail(client.getUser().getEmail());
        bookedUserDTO.setClientPhoneNumber(client.getMobileNumber());
        bookedUserDTO.setAvailableTime(bookingAvailability.getAvailabilityTime());
        return bookedUserDTO;
    }

    @Override
    public ByteArrayInputStream downloadExcelFileOfCounsellors() throws IOException {
        List<Counsellor> counsellorList = counsellorRepository.findAll();
        return excelFileCounsellor.generateExcelFile(counsellorList);
    }

    @Override
    public ByteArrayInputStream downloadExcelFileOfClient() throws IOException {
        List<Client> clientList = clientRepository.findAll();
        return excelFileClient.generateExcelFile(clientList);
    }
}
