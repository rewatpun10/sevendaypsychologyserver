package com.sevenDayPsychologyServer.enumeration;

public enum Roles {
    ADMIN, CLIENT, COUNSELLOR
}
