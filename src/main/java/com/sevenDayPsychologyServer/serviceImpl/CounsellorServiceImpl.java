package com.sevenDayPsychologyServer.serviceImpl;

import com.sevenDayPsychologyServer.entities.Counsellor;
import com.sevenDayPsychologyServer.repository.CounsellorRepository;
import com.sevenDayPsychologyServer.service.CounsellorService;
import org.springframework.beans.factory.annotation.Autowired;

public class CounsellorServiceImpl implements CounsellorService {

    @Autowired
    private CounsellorRepository counsellorRepository;
    @Override
    public Counsellor createCounsellor(Counsellor counsellor) {
        return counsellorRepository.save(counsellor);
    }
}
