package com.sevenDayPsychologyServer.repository;

import com.sevenDayPsychologyServer.entities.Client;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClientRepository extends JpaRepository<Client, Long> {
    Client findClientByUserId(Long Id);
}
