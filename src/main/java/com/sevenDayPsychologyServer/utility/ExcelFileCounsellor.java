package com.sevenDayPsychologyServer.utility;

import com.sevenDayPsychologyServer.entities.Counsellor;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

@Service
public class ExcelFileCounsellor {

    public ByteArrayInputStream generateExcelFile(List<Counsellor> counsellors) throws IOException {
        Workbook workbook = new XSSFWorkbook();
        ByteArrayOutputStream  outputStream = new ByteArrayOutputStream();

        Sheet sheet = workbook.createSheet("Counsellors");
        sheet.setColumnWidth(0, 3000);


        Row header = sheet.createRow(0);

        CellStyle headerStyle = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setBold(true);
        font.setColor(IndexedColors.BLUE.getIndex());
        headerStyle.setFont(font);

        Cell headerCell = header.createCell(0);
        headerCell.setCellValue("First Name");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(1);
        headerCell.setCellValue("Middle Name");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(2);
        headerCell.setCellValue("Last Name");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(3);
        headerCell.setCellValue("Phone Number");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(4);
        headerCell.setCellValue("Description");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(5);
        headerCell.setCellValue("Email");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(6);
        headerCell.setCellValue("Status");
        headerCell.setCellStyle(headerStyle);
        //writing the content of the table with a different style
        CellStyle style = workbook.createCellStyle();
        style.setWrapText(true);


        Iterator<Counsellor> iterator = counsellors.iterator();

        int rowIndex = 1;
        while(iterator.hasNext()){
            Counsellor counsellor = iterator.next();
            Row row = sheet.createRow(rowIndex++);
            Cell cell0 = row.createCell(0);
            cell0.setCellValue(counsellor.getFirstName() == null ? "No info for First Name" : counsellor.getFirstName());
            Cell cell1 = row.createCell(1);
            cell1.setCellValue(counsellor.getMiddleName() == null ? " No info for Middle Name" : counsellor.getMiddleName());
            Cell cell2 = row.createCell(2);
            cell2.setCellValue(counsellor.getLastName() == null ? " No info for Last Name" : counsellor.getLastName());
            Cell cell3 = row.createCell(3);
            cell3.setCellValue(counsellor.getMobileNumber() == null ? " No info for mobile number": counsellor.getMobileNumber());
            Cell cell4 = row.createCell(4);
            cell4.setCellValue(counsellor.getDescription() == null ? " No info for description " : counsellor.getDescription());
            Cell cell5 = row.createCell(5);
            cell5.setCellValue(counsellor.getUser().getEmail());
            Cell cell6 = row.createCell(5);
            cell6.setCellValue(counsellor.getUser().isStatus() == true ? "Deactivated User" : "Active User");
        }
        workbook.write(outputStream);
        workbook.close();
        return new ByteArrayInputStream(outputStream.toByteArray());

    }
}
