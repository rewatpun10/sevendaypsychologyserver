package com.sevenDayPsychologyServer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SevenDayPsychologyServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SevenDayPsychologyServerApplication.class, args);
	}

}
