package com.sevenDayPsychologyServer.service;

import com.sevenDayPsychologyServer.entities.Counsellor;

public interface CounsellorService {
    Counsellor createCounsellor(Counsellor counsellor);
}
