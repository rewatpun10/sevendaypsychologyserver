package com.sevenDayPsychologyServer.utility;

import com.sevenDayPsychologyServer.entities.Client;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.Iterator;
import java.util.List;

@Service
public class ExcelFileClient {

    public ByteArrayInputStream generateExcelFile(List<Client> clients) throws IOException {
        Workbook workbook = new XSSFWorkbook();
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        Sheet sheet = workbook.createSheet("Client");
        sheet.setColumnWidth(0, 3000);

        Row header = sheet.createRow(0);

        CellStyle headerStyle = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setBold(true);
        font.setColor(IndexedColors.BLUE.getIndex());
        headerStyle.setFont(font);
        headerStyle.setFont(font);

        Cell headerCell = header.createCell(0);
        headerCell.setCellValue("First Name");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(1);
        headerCell.setCellValue("Middle Name");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(2);
        headerCell.setCellValue("Last Name");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(3);
        headerCell.setCellValue("Phone Number");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(4);
        headerCell.setCellValue("Email");
        headerCell.setCellStyle(headerStyle);

        headerCell = header.createCell(5);
        headerCell.setCellValue("Status");
        headerCell.setCellStyle(headerStyle);

        //writing the content of the table with a different style
        CellStyle style = workbook.createCellStyle();
        style.setWrapText(true);

        Iterator<Client> iterator = clients.iterator();

        int rowIndex = 1;
        while(iterator.hasNext()){
            Client client = iterator.next();
            Row row = sheet.createRow(rowIndex++);
            Cell cell0 = row.createCell(0);
            cell0.setCellValue(client.getFirstName() == null ? "No info for First Name" : client.getFirstName());
            Cell cell1 = row.createCell(1);
            cell1.setCellValue(client.getMiddleName() == null ? "No info for Middle Name" : client.getMiddleName());
            Cell cell2 = row.createCell(2);
            cell2.setCellValue(client.getLastName() == null ? "No info for Last Name" : client.getLastName());
            Cell cell3 = row.createCell(3);
            cell3.setCellValue(client.getMobileNumber()== null ? "No info for Mobile Number" : client.getMobileNumber());
            Cell cell4 = row.createCell(4);
            cell4.setCellValue(client.getUser().getEmail()== null ? "No info for Email" : client.getUser().getEmail());
            Cell cell5 = row.createCell(5);
            cell5.setCellValue(client.getUser().isStatus() == true ? "Deactivated User" : "Active User");
        }

        workbook.write(outputStream);
        workbook.close();
        return new ByteArrayInputStream(outputStream.toByteArray());
    }
}
