package com.sevenDayPsychologyServer.service;

import com.sevenDayPsychologyServer.dto.ClientBookingDTO;
import com.sevenDayPsychologyServer.entities.BookedAppointment;
import com.sevenDayPsychologyServer.entities.BookingAvailability;

import java.util.List;

public interface BookedService {
    BookedAppointment saveClientBooking(ClientBookingDTO clientBookingDTO);

    List<BookingAvailability> getBookedDetails(Long clientId);
}
