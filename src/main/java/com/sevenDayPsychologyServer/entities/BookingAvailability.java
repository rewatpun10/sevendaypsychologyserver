package com.sevenDayPsychologyServer.entities;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "booking_availability")
public class BookingAvailability {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "booking_availability_id")
    private Long id;

    private Long counsellorId;

    private String availabilityTime;

    private boolean status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCounsellorId() {
        return counsellorId;
    }

    public void setCounsellorId(Long counsellorId) {
        this.counsellorId = counsellorId;
    }

    public String getAvailabilityTime() {
        return availabilityTime;
    }

    public void setAvailabilityTime(String availabilityTime) {
        this.availabilityTime = availabilityTime;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
